package br.com.alura.microservice.loja.dto;

public class ItemPedido extends ItemDaCompraDTO {

	private ProdutoDTO produto;

	public ProdutoDTO getProduto() {
		return produto;
	}

	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}
	
}
